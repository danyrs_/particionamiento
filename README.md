# Particionamiento
Proyecto de simulación (https://gitlab.com/danyrs_/particionamiento.git) de particiones de datos en un sistema de gestión de bases de datos relacional de código abierto: PostgreSQL. Utilizando los datos de calificaciones de películas recopilados del sitio web MovieLens (https://grouplens.org/datasets/movielens/).

## Instalación
Para la ejecución de este proyecto es necesario tener instalado Python3 y PostgreSQL.
Es necesario modificar los parámetros de conexión a la base de datos en el archivo db.ini. Tanto para la conexión a la bd de postgres (por defecto), como a la que será utilizada en la simulación (en mi caso 'i6_tp1').

## Ejecución
El archivo de ratings, descargado de la página de MovieLens, debe denominarse 'ratings.csv' y ubicarse en el directorio raíz. Es necesario verificar que el símbolo separador sea la coma (','). Para ejecutar, basta con:
```
python3 ./partition.py
```

## Utilización
Una vez ejecutado el programa, le saldrán las opciones disponibles:
```
[1] Conexión: Creará la BD, obtendrá la conexión e insertará los datos del archivo.
[2] Partición: Creará las particiones siguiendo el esquema Round Robin. Será necesario ingresar el número de particiones.
[3] Inserción: Se insertará un nuevo registro. Ingresar siguiendo el formato: user_id,movie_id,rating.
[4] Eliminar: Borrará las particiones, y la tabla general.
[q] Salir
```
