#!/usr/bin/python

import sys
import psycopg2
import numbers
from datetime import datetime
from config import config, configIni

DATABASE_NAME = "i6_tp1"
RATING_TABLE_NAME = "rating"
RATING_FILE_PATH = "ratings.csv"


def connectDB():
    # Método de conexión a la Base de Datos: i6_tp1.

    conn = None
    try:
        # Leer parámetros de configuración.
        params = config()

        # Conexión a la BD.
        conn = psycopg2.connect(**params)
        return conn

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if conn is not None:
            conn.close()
            print("Conexión a BD cerrada.")


def createDB():
    # Método de creación de la Base de Datos: i6_tp1.

    conn = None
    try:
        # Leer parámetros de configuración.
        params = configIni()

        # Conexión a la BD y creación de Cursor.
        conn = psycopg2.connect(**params)
        cur = conn.cursor()

        # Verificación de existencia de la Base de Datos.
        cur.execute(
            "SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname='%s'"
            % (DATABASE_NAME,)
        )
        count = cur.fetchone()[0]

        # Creación de la Base de Datos.
        if count == 0:
            cur.execute("CREATE DATABASE %s" % (DATABASE_NAME,))

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if conn is not None:
            conn.close()
            print("Conexión de BD cerrada.")
    finally:
        # Cerrar cursor y conexión.
        cur.close()
        conn.close()


def insertFromFile():
    # Método de insercion de datos a partir de archivo.

    # Conexión a la BD y creación de Cursor.
    conn = connectDB()
    cur = conn.cursor()

    # Creación de la tabla de ratings.
    cur.execute("DROP TABLE IF EXISTS " + RATING_TABLE_NAME)
    cur.execute(
        "CREATE TABLE "
        + RATING_TABLE_NAME
        + ' (user_id int8 NULL, movie_id int8 NULL, rating numeric(2,1) NULL, "timestamp" int8 NULL);'
    )

    # Leer del archivo en modo sólo lectura, y copiar los datos del archivo a la tabla.
    with open(RATING_FILE_PATH, "r") as f:
        next(f)  # Saltar el header.
        cur.copy_from(f, RATING_TABLE_NAME, sep=",")

    conn.commit()
    print("Conexión realizada correctamente.")
    # Cerrar cursor y conexión.
    cur.close()
    conn.close()


def roundrobinpartition(nParticiones):
    # Método de partición round robin.

    name = "rrobin_part"
    try:

        global roundRobinPartitions
        roundRobinPartitions = int(nParticiones)

        # Verificar si la cantidad de particiones pasada como parámetro es un número.
        if not isinstance(nParticiones, numbers.Integral) or nParticiones < 0:
            return

        # Conexión a la BD y creación de Cursor.
        conn = connectDB()
        cursor = conn.cursor()

        # Verificar si la tabla de ratings se encuentra cargada.
        cursor.execute(
            "SELECT * FROM information_schema.tables WHERE table_name='%s'"
            % (RATING_TABLE_NAME)
        )
        if not bool(cursor.rowcount):
            print("La tabla no se encuentra cargada.")
            return

        # Obtener todos los registros de la taba rating.
        cursor.execute("SELECT * FROM %s" % RATING_TABLE_NAME)
        rows = cursor.fetchall()

        tableNum = 0
        # Particionar.
        while tableNum < nParticiones:
            newTableName = name + str(tableNum)
            cursor.execute(
                'CREATE TABLE IF NOT EXISTS %s(user_id int8 NULL, movie_id int8 NULL, rating numeric(2,1) NULL, "timestamp" int8 NULL)'
                % (newTableName)
            )
            tableNum += 1

        lastInserted = 0
        # Insertar en las particiones.
        for row in rows:
            newTableName = name + str(lastInserted)
            cursor.execute(
                "INSERT INTO %s(user_id, movie_id, rating, timestamp) VALUES(%d, %d, %f, %s)"
                % (newTableName, row[0], row[1], row[2], row[3])
            )
            lastInserted = (lastInserted + 1) % nParticiones

        global lastRoundRobinTable
        # Almacenar índice de última inserción.
        lastRoundRobinTable = int(lastInserted)
        print("Partición round robin completada.")
        conn.commit()

    except psycopg2.DatabaseError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    except IOError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()


def roundRobinInsert(user_id, movie_id, rating, timestamp):
    # Método para insertar teniendo en cuenta round robin.

    try:
        global roundRobinPartitions
        # Verificar el número de particiones creadas.
        if (
            roundRobinPartitions < 0
            or not isinstance(roundRobinPartitions, numbers.Integral)
            or rating < 0
        ):
            return

        # Conexión a la BD y creación de Cursor.
        conn = connectDB()
        cursor = conn.cursor()

        # Verificar si la tabla de ratings se encuentra cargada.
        cursor.execute(
            "SELECT * FROM information_schema.tables WHERE table_name='%s'"
            % (RATING_TABLE_NAME)
        )
        if not bool(cursor.rowcount):
            print("La tabla no se encuentra cargada.")
            return

        global lastRoundRobinTable
        name = "rrobin_part" + str(lastRoundRobinTable)

        # Insertar teniendo en cuenta el último registro insertado.
        cursor.execute(
            "INSERT INTO %s(user_id, movie_id, rating, timestamp) VALUES(%d, %d, %f, %s)"
            % (name, user_id, movie_id, rating, timestamp)
        )
        lastRoundRobinTable = (lastRoundRobinTable + 1) % roundRobinPartitions

        print("Insercion realizada correctamente, en la tabla: " + name + ".")
        conn.commit()

    except psycopg2.DatabaseError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    except IOError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()


def delete():
    # Método para eliminar las particiones.

    try:
        # Conexión a la BD y creación de Cursor.
        conn = connectDB()
        cursor = conn.cursor()

        # Obtener todas las tablas del esquema public.
        cursor.execute(
            "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
        )
        tables = cursor.fetchall()

        # Eliminar las tablas en casacada.
        for table_name in tables:
            cursor.execute("DROP TABLE %s CASCADE" % (table_name[0]))

        print("Borrado realizado correctamente.")
        conn.commit()

    except psycopg2.DatabaseError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    except IOError as e:
        if conn:
            conn.rollback()
        print("Error %s" % e)
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()


def init():
    # Inicio.

    # Valor inicial para la opción, que sea diferente a q, y entre
    opcion = ""
    while opcion != "q":
        # Opciones.
        print("\n[1] Conexión")
        print("[2] Partición")
        print("[3] Inserción")
        print("[4] Eliminar")
        print("[q] Salir")

        # Opción que el usuario debe ingresar.
        opcion = input("\n¿Qué desea hacer? ")

        # Casos a tener en cuenta la opción.
        if opcion == "1":
            createDB()
            insertFromFile()
        elif opcion == "2":
            # Ingresar el número de particiones, casteando su valor.
            particiones = input("\nIngresar nro. de particiones: ")
            roundrobinpartition(int(particiones))
        elif opcion == "3":
            # Ingresar el registro.
            registro = input(
                "\nIngresar registro a insertar -> de esta manera (user_id,movie_id,rating): "
            )

            # Obtener los valores, separados por la coma.
            result = [x.strip() for x in registro.split(",")]

            # Pasar como parámetros los valores casteando el tipo de dato.
            roundRobinInsert(
                int(result[0]),
                int(result[1]),
                float(result[2]),
                datetime.now().timestamp(),
            )
        elif opcion == "4":
            delete()
        elif opcion == "q":
            print("\nMuchas gracias.\n")
        else:
            print("\nNo es una opción válida. Pruebe de vuelta, por favor.\n")


if __name__ == "__main__":
    init()
